alphabet = ["a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z"]
message = input("what message would you like to encrypt? ").lower()
cipher = int(input("what cipher would you like to use? (1-25) "))
encryption = []
for x in range(len(message)):
    if message[x] in alphabet:
        for y in range(len(alphabet)):
            if message[x] == alphabet[y]:
                encryption.append(alphabet[y + cipher - 26])
    elif message[x] == " ":
        encryption.append(" ")
    else:
        encryption.append(message[x])

final_encryption = ""
for w in range(len(encryption)):
    final_encryption = final_encryption + encryption[w]
print(final_encryption)
    
