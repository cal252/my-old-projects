#sudoku display and solver

# Uses backtracking

board = [[0,0,3,8,9,0,5,0,0],
[6,1,8,0,0,0,0,9,0],
[0,0,0,0,0,0,2,6,0],
[3,0,0,0,0,0,0,0,0],
[0,4,5,0,0,7,0,0,6],
[2,0,0,0,4,3,9,0,0],
[0,0,0,0,0,4,0,0,0],
[0,8,0,0,0,0,0,5,0],
[0,5,0,0,6,0,8,0,2]] # master 951 

def change_board(a_board):
	for i in range(len(a_board)):
		for j in range(len(a_board[i])):
			if a_board[i][j] == 0:
				a_board[i][j] = " "
	#return a_board

def display(board):
	for i in range(0,7,3):
		print("-"*19)
		for j in range(3):
			print("|{0} {1} {2}|{3} {4} {5}|{6} {7} {8}|".format(board[i+j][0],board[i+j][1],board[i+j][2],board[i+j][3],board[i+j][4],board[i+j][5],board[i+j][6],board[i+j][7],board[i+j][8]))
	print("-"*19)

def check_zeros(board):
	for i in range(9):
		for j in range(9):
			if board[i][j] == 0:
				return [i, j]     #where i is the row and j is the column
	return None

def check_row(board, pos):
	value = board[pos[0]][pos[1]]
	count = 0
	for j in range(9):
		if value == board[pos[0]][j]:
			count += 1
	if count > 1:
		return True
	else:
		return False

def check_column(board, pos):
	value = board[pos[0]][pos[1]]
	count = 0
	for i in range(9):
		if value == board[i][pos[1]]:
			count += 1
	if count > 1:
		return True
	else:
		return False

def check_cell(board, pos):
	value = board[pos[0]][pos[1]]
	count = 0
	i_start = int(pos[0]/3)
	j_start = int(pos[1]/3)
	i_start *= 3
	j_start *= 3
	for i in range(i_start, i_start + 3, 1):
		for j in range(j_start, j_start + 3, 1):
			if value == board[i][j]:
				count += 1
	if count > 1:
		return True
	else:
		return False

def solve(board, pos):
	global num
	for k in range(9):
		board[pos[0]][pos[1]] += 1
		occurance = 0
		occurance += check_row(board, pos)
		occurance += check_column(board, pos)
		occurance += check_cell(board, pos)
		if occurance == 0:
			_next = check_zeros(board)
			if _next == None:
				print("Solution {}.".format(num))
				num += 1
				display(board)
				#input("More?")
				return True
			else:
				solve(board, _next)
	board[pos[0]][pos[1]] = 0
	
num = 1

display(board)
pos = check_zeros(board)
solve(board, pos)
#change_board(board)
#display(board)
#display(original)

