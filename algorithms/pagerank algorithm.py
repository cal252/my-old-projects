#pagerank algorithm

#PR(A) = (1-d) + d(PR(T1)/C(T1) + ... + PR(Tn)/C(Tn))

#PR(A) pagerank of page A
#PR(Ti) pagerank of pages Ti, which are pages which link to page A
#C(Ti) the number of outbound links on page Ti
#d is the damping factor which can be set between 0 and 1 (usually 0.85)

damping_factor = 0.85

class site(object):
	def __init__(self):
		self.rank = 1
		self.in_links = []
		self.out_links = 0

	def add_in_link(self, a_site):
		self.in_links.append(a_site)
		a_site.out_links += 1



def pagerank(a_site):
	a = 1 - damping_factor
	for i in range(len(a_site.in_links)):
		a += damping_factor * (a_site.in_links[i].rank / a_site.in_links[i].out_links)
	a_site.rank = a


a = site()
b = site()
c = site()
d = site()

site_list = [a,b,c,d]

d.add_in_link(b)
d.add_in_link(c)
c.add_in_link(b)
c.add_in_link(a)
b.add_in_link(a)
a.add_in_link(d)

for i in range(100000):
	for i in range(len(site_list)):
		pagerank(site_list[i])


for i in range(len(site_list)):
	print(site_list[i].rank)

#print("a")
#print(a.in_links)
#print(a.out_links)

#print("b")
#print(b.in_links[0].in_links)
#print(b.out_links)

#print("c")
#print(c.in_links)
#print(c.out_links)

#print("d")
#print(d.in_links)
#print(d.out_links)
