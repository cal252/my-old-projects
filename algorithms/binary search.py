#binary search
import random

a_list = []

for i in range(100000):
	a_list.append(random.randint(0, 10000))

a_list.sort()
#print(a_list)

search = 5841
location = 0
searches = 0

while True:
	#print(a_list)
	searches += 1
	if search == a_list[int(len(a_list)/2)]:
		location += int(len(a_list)/2) + 1
		print("{0} in position {1}".format(search, location))
		print("found in {0} searches".format(searches))
		break
	elif len(a_list) == 1 and search != a_list[0]:
		print("{0} is not in the list".format(search))
		break
	elif search < a_list[int(len(a_list)/2)]:
		a_list = a_list[:int(len(a_list)/2)]
	elif search > a_list[int(len(a_list)/2)]:
		#print(location)
		location += int(len(a_list)/2)
		a_list = a_list[int(len(a_list)/2):]

