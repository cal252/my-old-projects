import math

def quadratic_solver():
    all_inputs = False
    while all_inputs == False:
        a = b = c = ""
        try:
            a = int(input("Enter a value for a (the x^2 term): "))
        except:
            pass
        try:
            b = int(input("Enter a value for b (the x term): "))
        except:
            pass
        try:
            c = int(input("Enter a value for c (the constant term): "))
        except:
            pass
        try:
            if type(a) == type(b) == type(c) == type(2):
                all_inputs = True
        except:
            pass
    if b**2 - 4*a*c > 0:
        x1 = (-b + (math.sqrt(b**2 - 4*a*c)))/(2*a)
        x2 = (-b - (math.sqrt(b**2 - 4*a*c)))/(2*a)
        x1 = str(x1)
        x2 = str(x2)
        print("two distinct real roots:\nx1 = " + x1 + "\nx2 = " + x2)
        return
    elif b**2 - 4*a*c == 0:
        x = -b/(2*a)
        x = str(x)
        print("equal roots:\nx = " + x)
        return
    else:
        real_part = -b/(2*a)
        complex_part = (math.sqrt((b**2 - 4*a*c) * -1))/(2*a)
        real_part = str(real_part)
        complex_part = str(complex_part)
        print("two complex roots:\nx1 = " + real_part + " + " + complex_part + "i" + "\nx2 = " + real_part + " - " + complex_part + "i")
        return 
        

quadratic_solver()