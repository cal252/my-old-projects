#application that graphs functions

import pygame, sys, random, time
from pygame.locals import *

pygame.init()

screen_width = 800
screen_height = 600

win = pygame.display.set_mode((screen_width, screen_height), 0, 32)
pygame.display.set_caption("Graphing app")

font = pygame.font.SysFont("comicsans", 12, True)

class functions(object):
    def __init__(self):
        pass

    def pi():
        """Returns the vale of pi."""
        pi = 3.141592653589793
        return pi

    def e():
        """Returns the value e."""
        i = 2**52
        e = (1 + (1/i))**i
        return e

    def fact(num):
        """Returns the factorial of a number x uo to 1023."""
        if num == 0:
            return 1
        elif num < 0:
            return "Error"
        else:
            x = num * functions.fact(num -1)
            return x

    def exp(x):
        """Gives the value of e to the power of the number x."""
        i = 0
        ans = 0
        for i in range(100):
            ans += (x**i)/functions.fact(i)
        return ans

    def sin(x):
        """Gives sine of the input x, where x is in radians."""
        sinx = 0
        i = 1
        sign = 1
        if x > functions.pi():
            while x > functions.pi():
                x -= 2*functions.pi()
        elif x < -1 * functions.pi():
            while x < -1 * functions.pi():
                x += 2*functions.pi()
        for j in range(50):
            sinx += (sign * (x**i))/functions.fact(i)
            i +=2
            sign *= -1
        sinx = round(sinx, 12)
        return sinx

    def cos(x):
        """Gives cosine of the input x, where x is in radians."""
        cosx = 0
        i = 0
        sign = 1
        if x > functions.pi():
            while x > functions.pi():
                x -= 2*functions.pi()
        elif x < -1 * functions.pi():
            while x < -1 * functions.pi():
                x += 2*functions.pi()
        for j in range(50):
            cosx += (sign * (x**i))/functions.fact(i)
            i +=2
            sign *= -1
        cosx = round(cosx, 12)
        return cosx

    def tan(x):
        """Gives the tangent of the input x, where x is in radians."""
        if functions.cos(x) == 0:
            return "error"
        else:
            tanx = functions.sin(x) / functions.cos(x)
            return tanx

    def ln(x):
        """Returns the natural log of the number x, where x is non negative."""
        if x <= 1:
            x -= 1 
            lnx = 0
            i = 1
            sign = 1
            for j in range(7):
                lnx += (sign * (x**i))/i
                i += 1
                sign *= -1
            lnx = round(lnx, 12)
            return lnx
        else:
            x = x**(-1)
            lnx = -1 * functions.ln(x)
            return lnx
            

    def sinh(x):
        """Gives the hyperbolic sine of the value x."""
        sinhx = (functions.exp(x) - functions.exp(-x))/2
        return sinhx

    def cosh(x):
        """Gives the hyperbolic cosine of the value x."""
        coshx = (functions.exp(x) + functions.exp(-x))/2
        return coshx

    def tanh(x):
        """Gives the hyperbolic tangent of the value x."""
        tanhx = (functions.exp(2*x) - 1)/(functions.exp(2*x) + 1)
        return tanhx

    def arcsin(x):
        pass

    def arccos(x):
        pass

    def arctan(x):
        pass

    def sqrt(x):
        pass

    def sqr(x):
    	return x**2

    def cube(x):
    	return x**3

    def quar(x):
    	return x**4

    def custom(x):
    	ans = x**2 + 2*x -3
    	return ans

    def custom2(x):
    	ans = 2*functions.sin(x) - functions.cos(x)
    	return ans

    def custom3(x):
    	ans = functions.sin(x - functions.pi()/2)
    	return ans


class graph(object):
	def __init__(self):
		self.origin = (int(screen_width/2), int(screen_height/2))
		self.incriment = 25
		self.function_coords = []

	def main(self):
		graph.draw(self)

	def draw(self):
		graph.draw_axis(self)
		graph.plot(self)

	def draw_axis(self):
		win.fill((225, 225, 225))
		for i in range(1, 40):
			pygame.draw.line(win, (200, 200, 200), (self.incriment * i, 0), (self.incriment * i, screen_height))
			pygame.draw.line(win, (200, 200, 200), (0, self.incriment * i), (screen_width, self.incriment * i))
			pygame.draw.line(win, (10, 10, 10), (self.incriment * i, self.origin[1] - 5), (self.incriment * i, self.origin[1] + 5)) #lines on x axis
			pygame.draw.line(win, (10, 10, 10), (self.origin[0] - 5, self.incriment * i), (self.origin[0] + 5, self.incriment * i)) #lines on y axis
		pygame.draw.line(win, (10, 10, 10), (self.origin[0], 0), (self.origin[0], screen_height))
		pygame.draw.line(win, (10, 10, 10), (0, self.origin[1]), (screen_width, self.origin[1]))
		for i in range(1, 16):
			text = str(i)
			text_font = font.render(text, 1, (10, 10, 10))
			win.blit(text_font, (self.origin[0] + i * self.incriment - int(text_font.get_width()/2), self.origin[1] + 10))
			win.blit(text_font, (self.origin[0] + 15, self.origin[1] - i * self.incriment - int(text_font.get_height()/2)))
		for i in range(1, 16):
			text = str(-i)
			text_font = font.render(text, 1, (10, 10, 10))
			win.blit(text_font, (self.origin[0] - i * self.incriment - int(text_font.get_width()/2), self.origin[1] + 10))
			win.blit(text_font, (self.origin[0] + 15, self.origin[1] + i * self.incriment - int(text_font.get_height()/2)))
		pygame.display.update()

	def convert_coords(self, x, y):
		"""co-ordinates to position of pixels"""
		new_x = self.origin[0] + x * self.incriment
		new_y = self.origin[1] - y * self.incriment
		return (new_x, new_y)

	def plot(self):
		coords = []
		for i in range(len(self.function_coords)):
			coords.append(graph.convert_coords(self, self.function_coords[i][0], self.function_coords[i][1]))
		for i in range(len(coords) - 1):
			pygame.draw.line(win, (40, 40, 40), (int(coords[i][0]), int(coords[i][1])), (int(coords[i + 1][0]), int(coords[i + 1][1])))
		pygame.display.update()



	def equation(self, function):
		for i in range(-200, 200):
			self.function_coords.append((i * 0.08, function(i * 0.08)))




graphing = graph()

#changes the equation that is graphed. can use the functions class defined above or make custom functions

####################################
graphing.equation(functions.exp)
####################################



graphing.main()


def run():
    while True:
        for event in pygame.event.get():
            if event.type == QUIT:
                pygame.quit()
                sys.exit()
        #graphing.main()

time.sleep(10)
#run()
