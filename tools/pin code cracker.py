import random

digits = []
exc_1 = []
exc_2 = []
exc_3 = []
exc_4 = []

correct_length = False
while correct_length == False:
    digit_1 = input("please enter the first digit for the pin code ")
    digit_2 = input("please enter the second digit for the pin code ")
    digit_3 = input("please enter the third digit for the pin code ")
    digit_4 = input("please enter the fourth digit for the pin code ")
    if len(digit_1) == 1 and len(digit_2) == 1 and len(digit_3) == 1 and len(digit_4) == 1:
        correct_length = True
    else:
        print("digits of pin code must be one digit long, please re enter the digits ")

digits.append(digit_1)
digits.append(digit_2)
digits.append(digit_3)
digits.append(digit_4)

exc_1.append(digit_2)
exc_1.append(digit_3)
exc_1.append(digit_4)

exc_2.append(digit_1)
exc_2.append(digit_3)
exc_2.append(digit_4)

exc_3.append(digit_1)
exc_3.append(digit_2)
exc_3.append(digit_4)

exc_4.append(digit_1)
exc_4.append(digit_2)
exc_4.append(digit_3)


print(digits)

same = 0

def find(List, digit):
    index = 0
    while index < 3:
        if List[index] == digit:
            global same
            same = same + 1
        index = index + 1
        
find(exc_1, digit_1)
same_1 = same
same = 0

find(exc_2, digit_2)
same_2 = same
same = 0 

find(exc_3, digit_3)
same_3 = same
same = 0

find(exc_4, digit_4)
same_4 = same
same = 0 

combinations = []
m = 0

similar = same_1 + same_2 + same_3 + same_4
if similar == 0:
    similar_multiplyer = 1
elif similar == 12:
    similar_multiplyer = 24
else:
    similar_multiplyer = similar

for x in range(int(24/similar_multiplyer)):
    new_found = False
    while new_found == False:
        number = random.randint(0, 3)
        number_1 = random.randint(0, 2)
        number_2 = random.randint(0, 1)
        
        combination = []
        combination.append(digits[number])
        del(digits[number])
        
        combination.append(digits[number_1])
        del(digits[number_1])
        
        combination.append(digits[number_2])
        del(digits[number_2])
        
        combination.append(digits[0])
        del(digits[0])

        new = combination in combinations

        if new == False:
            combinations.append(combination)
            new_found = True
            digits.append(digit_1)
            digits.append(digit_2)
            digits.append(digit_3)
            digits.append(digit_4)
            m = m + 1
        else:
            new_found = False
            digits.append(digit_1)
            digits.append(digit_2)
            digits.append(digit_3)
            digits.append(digit_4)
            m = m + 1

combinations.sort()

def results(number):
        print(combinations[number][0], combinations[number][1], combinations[number][2], combinations[number][3])

for x in range(int(24/similar_multiplyer)):
    results(x)

print(m, "attempts to find result")
