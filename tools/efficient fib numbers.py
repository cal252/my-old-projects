import math

def fib(n):
    fib_number = round((1/(math.sqrt(5))) * ((1+math.sqrt(5))/2)**n - (1/(math.sqrt(5))) * ((1-math.sqrt(5))/2)**n, 1)
    return fib_number


print(fib(20))