This directory contains some different scripts for solving some problems (mathematical). Efficient fib numbers calculates fibonacci  numbers using a mathematical formula. Graphing applciation plots mathematical equations using math functions like e^x. Pin code cracker takes in 4 numbers and bruit forces the combination. Quadratic solver uses the quadratic formula to solve quadratic equations.

Date Last Modified:
efficient fib numbers - Feb 2020
graphing application - Mar 2021
pin code cracker - Sep 2018
quadratic solver - Mar 2020
