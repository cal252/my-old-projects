import time, pygame, sys
from pygame.locals import *


pygame.init()


screen_width = 800
screen_height = 600

win = pygame.display.set_mode((screen_width, screen_height), 0, 32)
pygame.display.set_caption("Alarm Clock")

class parts(object):

	def __init__(self, x, y, rotation, width, height, active_colour, inactive_colour):
		"""parts(x, y, rotation, width, height, active_colour, inactive_colour"""
		self.x = x
		self.y = y
		self.rotation = rotation
		self.width = width
		self.height = height
		self.extra_height = int(height / 6)
		self.colour = inactive_colour
		self.active_colour = active_colour
		self.inactive_colour = inactive_colour

	def draw(self):
		if self.rotation == "vert":
			pygame.draw.rect(win, self.colour, (self.x, self.y, self.width, self.height))
			pygame.draw.polygon(win, self.colour, ((self.x, self.y), (self.x + self.width, self.y), (self.x + int(self.width / 2), self.y - self.extra_height)))
			pygame.draw.polygon(win, self.colour, ((self.x, self.y + self.height), (self.x + self.width, self.y + self.height), (self.x + int(self.width / 2), self.y + self.height + self.extra_height)))
		elif self.rotation == "horiz":
			pygame.draw.rect(win, self.colour, (self.x, self.y, self.height, self.width))
			pygame.draw.polygon(win, self.colour, ((self.x, self.y), (self.x - self.extra_height, self.y + int(self.width / 2)), (self.x, self.y + self.width)))
			pygame.draw.polygon(win, self.colour, ((self.x + self.height, self.y), (self.x + self.height + self.extra_height, self.y + int(self.width / 2)), (self.x + self.height, self.y + self.width)))

	def new_pos(self, new_x, new_y):
		self.x = new_x
		self.y = new_y

	def change_pos(self, add_x, add_y):
		self.x += add_x
		self.y += add_y

	def change_colour(self, x):
		if x == 1:
			self.colour = self.active_colour
		elif x == 0:
			self.colour = self.inactive_colour
		else:
			print("Error")

class clock(object):

	def __init__(self, x = 150, y = 200, width = 500, height = 200):
		self.x = x
		self.y = y
		self.width = width
		self.height = height
		self.digits = [0, 0, 0, 0, 0, 0]
		#parts starting top right of the 8 bit display going clockwise then the bit in the middle
		self.part_1 = parts(230, 260, "vert", 10, 30, (50, 50, 50), (240, 240, 240))
		self.part_2 = parts(230, 310, "vert", 10, 30, (50, 50, 50), (240, 240, 240))
		self.part_3 = parts(195, 345, "horiz", 10, 30, (50, 50, 50), (240, 240, 240))
		self.part_4 = parts(180, 310, "vert", 10, 30, (50, 50, 50), (240, 240, 240))
		self.part_5 = parts(180, 260, "vert", 10, 30, (50, 50, 50), (240, 240, 240))
		self.part_6 = parts(195, 245, "horiz", 10, 30, (50, 50, 50), (240, 240, 240))
		self.part_7 = parts(195, 295, "horiz", 10, 30, (50, 50, 50), (240, 240, 240))
		self.part_list = [self.part_1, self.part_2, self.part_3, self.part_4, self.part_5, self.part_6, self.part_7]

	def draw(self):
		pygame.draw.rect(win, (0, 0, 0), (self.x, self.y, self.width, self.height), 2)
		for i in range(len(self.part_list)):
			self.part_list[i].draw()
		pygame.draw.circle(win, (50, 50, 50), (322, 280), 5)
		pygame.draw.circle(win, (50, 50, 50), (322, 320), 5)
		pygame.draw.circle(win, (50, 50, 50), (478, 280), 5)
		pygame.draw.circle(win, (50, 50, 50), (478, 320), 5)
		pygame.display.update()

	def get_time(self):
		current_time = time.ctime()
		current_time = current_time.split(" ")
		current_time = current_time[3]
		self.digits = []
		for i in range(0, 7, 3):
			self.digits.append(int(current_time[i]))
			self.digits.append(int(current_time[i+1]))

	def get_colour_map(self, digit):
		colour_map = number_dict[digit]
		return colour_map

	def set_colour_map(self, colour_map):
		for i in range(len(self.part_list)):
			self.part_list[i].change_colour(colour_map[i])

	def change_part_pos(self, pos_change):
		for i in range(len(self.part_list)):
			self.part_list[i].change_pos(pos_change, 0) 
	

	def run(self):
		win.fill((255, 255, 255))
		pos_change = (70, 85, 70, 85, 70, -380)
		clock.get_time(self)
		for i in range(len(self.digits)):
			colour_map = clock.get_colour_map(self, self.digits[i])
			clock.set_colour_map(self, colour_map)
			clock.draw(self)
			clock.change_part_pos(self, pos_change[i])

number_dict = {0:(1,1,1,1,1,1,0), 1:(1,1,0,0,0,0,0), 2:(1,0,1,1,0,1,1), 3:(1,1,1,0,0,1,1), 4:(1,1,0,0,1,0,1), 5:(0,1,1,0,1,1,1), 6:(0,1,1,1,1,1,1), 7:(1,1,0,0,0,1,0), 8:(1,1,1,1,1,1,1), 9:(1,1,1,0,1,1,1)}

main_clock = clock()

def main():
	win.fill((255, 255, 255))
	main_clock.run()

FPS = 1

c = pygame.time.Clock()

while True:
    c.tick(FPS)
    for event in pygame.event.get():
        if event.type == QUIT:
            pygame.quit()
            sys.exit()
    main()
