#calculator app
import pygame
import math
import sys
from pygame.locals import *

pygame.init()

screen_width = 320            #initialising pygame and the window the game will be displayed on
screen_height = 520

win = pygame.display.set_mode((screen_width, screen_height), 0, 32)
pygame.display.set_caption("Calculator")                             #creating the window title

font = pygame.font.SysFont("Bebas Neue", 40)

class button(object):
    def __init__(self, x, y, width, height, colour, hover_colour, text, button_type):
        self.x = x                              #x and y co-ordinates of the button
        self.y = y
        self.width = width
        self.height = height                #width and height of the button
        self.colour = colour
        self.hover_colour = hover_colour
        self.text = text                                #button colours and text inside
        self.button_type = button_type


    def draw(self):
        mouse_pos = pygame.mouse.get_pos()
        if mouse_pos[0] >= self.x and mouse_pos[0] <= self.x + self.width and mouse_pos[1] >= self.y and mouse_pos[1] <= self.y + self.height:
            pygame.draw.rect(win, self.hover_colour, (self.x, self.y, self.width, self.height))
        else:
            pygame.draw.rect(win, self.colour, (self.x, self.y, self.width, self.height))                                       #draws  the buttons with the text inside them
        button_text = font.render(self.text, 1, (255, 255, 255))
        button_text_width = button_text.get_width()
        button_text_height = button_text.get_height()
        if self.text == "_":
            win.blit(button_text, (int(self.x + self.width/2 - (button_text_width/2)), int(self.y + self.height/2 - (button_text_height/2) - 10)))
        else:
            win.blit(button_text, (int(self.x + self.width/2 - (button_text_width/2)), int(self.y + self.height/2 - (button_text_height/2))))


    def find_clicked(self):
        if event.type == pygame.MOUSEBUTTONDOWN:
            click = pygame.mouse.get_pos()
            if click[0] >= self.x and click[0] <= self.x + self.width and click[1] >= self.y and click[1] <= self.y + self.height:          #determines if a button has been clicked and returns true if it has
                #print("click!!!!!")
                return self.text

#number buttons
zero_button = button(4, 440,154, 75, (88, 88, 88), (120, 120, 120), "0", "number")
one_button = button(4, 360, 75, 75, (88, 88, 88), (120, 120, 120), "1", "number")
two_button = button(83, 360, 75, 75, (88, 88, 88), (120, 120, 120), "2", "number")
three_button = button(162, 360, 75, 75, (88, 88, 88), (120, 120, 120), "3", "number")
four_button = button(4, 280, 75, 75, (88, 88, 88), (120, 120, 120), "4", "number")
five_button = button(83, 280, 75, 75, (88, 88, 88), (120, 120, 120), "5", "number")
six_button = button(162, 280, 75, 75, (88, 88, 88), (120, 120, 120), "6", "number")
seven_button = button(4, 200, 75, 75, (88, 88, 88), (120, 120, 120), "7", "number")
eight_button = button(83, 200, 75, 75, (88, 88, 88), (120, 120, 120), "8", "number")
nine_button = button(162, 200, 75, 75, (88, 88, 88), (120, 120, 120), "9", "numer")
dot_button = button(162, 440, 75, 75, (88, 88, 88), (120, 120, 120), ".", "number")

number_buttons_list = [zero_button, one_button, two_button, three_button, four_button, five_button,
                       six_button, seven_button, eight_button, nine_button, dot_button]

#opperation buttons
equals_button = button(241, 440, 75, 75, (255, 132, 0), (255, 176, 92), "=", "opperation")
add_button = button(241, 360, 75, 75, (255, 132, 0), (255, 176, 92), "+", "opperation")
minus_button = button(241, 280, 75, 75, (255, 132, 0), (255, 176, 92), "-", "opperation")
multiply_button = button(241, 200, 75, 75, (255, 132, 0), (255, 176, 92), "x", "opperation")
divide_button = button(241, 120, 75, 75, (255, 132, 0), (255, 176, 92), "/", "opperation")

opperation_buttons_list = [equals_button, add_button, minus_button, multiply_button, divide_button]

#function buttons
ac_button = button(4, 120, 75, 75, (140, 140, 140), (182, 182, 182), "AC", "function")
plus_minus_button = button(83, 120, 75, 75, (140, 140, 140), (182, 182, 182), "+/-", "function")
percent_button = button(162, 120, 75, 75, (140, 140, 140), (182, 182, 182), "%", "function")

function_buttons_list = [ac_button, plus_minus_button, percent_button]

all_buttons = number_buttons_list + opperation_buttons_list + function_buttons_list

def my_e():
    i = 2**52
    e = (1 + (1/i))**i
    return e

def fact(num):
    if num == 0:
        return 1
    elif num < 0:
        return "Error"
    else:
        x = num * fact(num -1)
        return x

def e_to_x(x):
    i = 0
    ans = 0
    for i in range(1000):
        ans += (x**i)/fact(i)
    return ans

def my_pi():
    pi = 3.141592653589793
    return pi

def my_sin(x): # in radians
    sin = 0
    i = 1
    sign = 1
    if x > my_pi():
        while x > my_pi():
            x -= 2*my_pi()
    elif x < -1 * my_pi():
        while x < -1 * my_pi():
            x += 2*my_pi()
    for j in range(50):
        sin += (sign * (x**i))/fact(i)
        i +=2
        sign *= -1
    sin = round(sin, 12)
    return sin

def my_cos(x): #in radians
    cos = 0
    i = 0
    sign = 1
    if x > my_pi():
        while x > my_pi():
            x -= 2*my_pi()
    elif x < -1 * my_pi():
        while x < -1 * my_pi():
            x += 2*my_pi()
    for j in range(50):
        cos += (sign * (x**i))/fact(i)
        i +=2
        sign *= -1
    cos = round(cos, 12)
    return cos

def main():
    global number_1, number_2, active, number_text, process
    for i in range(len(all_buttons)):
        x = all_buttons[i].find_clicked()
        if x != None:
            if active[0] == "Error" and x in ["1", "2", "3", "4", "5", "6", "7", "8", "9", "0", "."]:
                if x == ".":
                    active[0] = "0" + x
                else:
                    active[0] = x
            elif  x in ["1", "2", "3", "4", "5", "6", "7", "8", "9", "0", "."] and number_text.get_width() < 280:
                if x == "." and "." not in active[0]:
                    active[0] = active[0] + x
                elif x in ["1", "2", "3", "4", "5", "6", "7", "8", "9", "0"]:
                    if len(active[0]) >= 1 and active[0] == "0":
                        active[0] = x
                    else:
                        active[0] = active[0] + x
            elif x == "AC":
                number_1 = "0"
                number_2 = "0"
                active = [number_1, number_2]
                process = ""
            elif x == "+/-":
                if "." in active[0]:
                    active[0] = str(float(active[0]) * -1)
                else:
                    active[0] = str(int(active[0]) * -1)
            elif x == "%":
                active[0] = str(round(float(active[0]) /100, 20))#fix error - when clicked does as is expected but
                
            elif x == "/":                                                          #allows to be added to at the end (concatonated)
                if process == "":
                    active.reverse()
                    process = "/"
                else:
                    process = "/"
            elif x == "x":
                if process == "":
                    active.reverse()
                    process = "x"
                else:
                    process = "x"
            elif x == "-":
                if process == "":
                    active.reverse()
                    process = "-"
                else:
                    process = "-"
            elif x == "+":
                if process == "":
                    active.reverse()
                    process = "+"
                else:
                    process = "+"
            elif x == "=":
                if process == "/":
                    active.reverse()
                    if active[1] != "0":
                        active[0] = str(round(float(active[0]) / float(active[1]), 16))
                        active[1] = "0"
                        process = ""
                        if float(active[0]) == int(float(active[0])):
                            active[0] = str(int(float(active[0])))
                    else:
                        active[0] = "Error"
                        active[1] = "0"
                        process = ""
                elif process == "x":
                    active.reverse()
                    active[0] = str(round(float(active[0]) * float(active[1]), 16))
                    active[1] = "0"
                    process = ""
                    if float(active[0]) == int(float(active[0])):
                            active[0] = str(int(float(active[0])))
                elif process == "-":
                    active.reverse()
                    active[0] = str(round(float(active[0]) - float(active[1]), 16))
                    active[1] = "0"
                    process = ""
                    if float(active[0]) == int(float(active[0])):
                            active[0] = str(int(float(active[0])))
                elif process == "+":
                    active.reverse()
                    active[0] = str(round(float(active[0]) + float(active[1]), 16))
                    active[1] = "0"
                    process = ""
                    if float(active[0]) == int(float(active[0])):
                            active[0] = str(int(float(active[0]))) 
    draw()


def draw():
    global number_1, number_2, active, number_text
    win.fill((14, 12, 14))
    pygame.draw.rect(win, (88, 88, 88), (0, 0, 320, 40))
    for i in range(len(all_buttons)):
        all_buttons[i].draw()
    #if active[0] == None:
        #number_text = font.render("0", 1, (255, 255, 255))
    #else:
    number_text = font.render(str(active[0]), 1, (255, 255, 255))
    win.blit(number_text, (310 - (number_text.get_width()), 70))
    pygame.display.update()
    

global number_1, number_2, active, process
number_1 = "0"
number_2 = "0"
active = [number_1, number_2]
process = ""

while True:                             #main loop that uses the FPS to determin the speed so it is the sameon every computer
    for event in pygame.event.get():
        if event.type == QUIT:
            pygame.quit()
            sys.exit()
        main()
