#calculator app Version 2
import pygame
import math
import sys
from pygame.locals import *

pygame.init()

screen_width = 557            #initialising pygame and the window the game will be displayed on
screen_height = 520

win = pygame.display.set_mode((screen_width, screen_height), 0, 32)
pygame.display.set_caption("Calculator")                             #creating the window title

font = pygame.font.SysFont("Bebas Neue", 40)

class button(object):
    def __init__(self, x, y, width, height, colour, hover_colour, text, button_type):
        self.x = x                              #x and y co-ordinates of the button
        self.y = y
        self.width = width
        self.height = height                #width and height of the button
        self.colour = colour
        self.hover_colour = hover_colour
        self.text = text                                #button colours and text inside
        self.button_type = button_type


    def draw(self):
        mouse_pos = pygame.mouse.get_pos()
        if mouse_pos[0] >= self.x and mouse_pos[0] <= self.x + self.width and mouse_pos[1] >= self.y and mouse_pos[1] <= self.y + self.height:
            pygame.draw.rect(win, self.hover_colour, (self.x, self.y, self.width, self.height))
        else:
            pygame.draw.rect(win, self.colour, (self.x, self.y, self.width, self.height))                                       #draws  the buttons with the text inside them
        button_text = font.render(self.text, 1, (255, 255, 255))
        button_text_width = button_text.get_width()
        button_text_height = button_text.get_height()
        if self.text == "_":
            win.blit(button_text, (int(self.x + self.width/2 - (button_text_width/2)), int(self.y + self.height/2 - (button_text_height/2) - 10)))
        else:
            win.blit(button_text, (int(self.x + self.width/2 - (button_text_width/2)), int(self.y + self.height/2 - (button_text_height/2))))


    def find_clicked(self):
        if event.type == pygame.MOUSEBUTTONDOWN:
            click = pygame.mouse.get_pos()
            if click[0] >= self.x and click[0] <= self.x + self.width and click[1] >= self.y and click[1] <= self.y + self.height:          #determines if a button has been clicked and returns true if it has
                #print("click!!!!!")
                return self.text
            
#number buttons
zero_button = button(4, 440,154, 75, (88, 88, 88), (120, 120, 120), "0", "number")
one_button = button(4, 360, 75, 75, (88, 88, 88), (120, 120, 120), "1", "number")
two_button = button(83, 360, 75, 75, (88, 88, 88), (120, 120, 120), "2", "number")
three_button = button(162, 360, 75, 75, (88, 88, 88), (120, 120, 120), "3", "number")
four_button = button(4, 280, 75, 75, (88, 88, 88), (120, 120, 120), "4", "number")
five_button = button(83, 280, 75, 75, (88, 88, 88), (120, 120, 120), "5", "number")
six_button = button(162, 280, 75, 75, (88, 88, 88), (120, 120, 120), "6", "number")
seven_button = button(4, 200, 75, 75, (88, 88, 88), (120, 120, 120), "7", "number")
eight_button = button(83, 200, 75, 75, (88, 88, 88), (120, 120, 120), "8", "number")
nine_button = button(162, 200, 75, 75, (88, 88, 88), (120, 120, 120), "9", "numer")
dot_button = button(162, 440, 75, 75, (88, 88, 88), (120, 120, 120), ".", "number")

number_buttons_list = [zero_button, one_button, two_button, three_button, four_button, five_button,
                       six_button, seven_button, eight_button, nine_button, dot_button]

#opperation buttons
equals_button = button(241, 440, 75, 75, (255, 132, 0), (255, 176, 92), "=", "opperation")
add_button = button(241, 360, 75, 75, (255, 132, 0), (255, 176, 92), "+", "opperation")
minus_button = button(241, 280, 75, 75, (255, 132, 0), (255, 176, 92), "-", "opperation")
multiply_button = button(241, 200, 75, 75, (255, 132, 0), (255, 176, 92), "x", "opperation")
divide_button = button(241, 120, 75, 75, (255, 132, 0), (255, 176, 92), "/", "opperation")

opperation_buttons_list = [equals_button, add_button, minus_button, multiply_button, divide_button]

#function buttons
ac_button = button(4, 120, 75, 75, (140, 140, 140), (182, 182, 182), "AC", "function")
plus_minus_button = button(83, 120, 75, 75, (140, 140, 140), (182, 182, 182), "+/-", "function")
percent_button = button(162, 120, 75, 75, (140, 140, 140), (182, 182, 182), "%", "function")

function_buttons_list = [ac_button, plus_minus_button, percent_button]

all_buttons = number_buttons_list + opperation_buttons_list + function_buttons_list


class functions(object):
    def __init__(self):
        pass

    def pi():
        """Returns the vale of pi."""
        pi = 3.141592653589793
        return pi

    def e():
        """Returns the value e."""
        i = 2**52
        e = (1 + (1/i))**i
        return e

    def fact(num):
        """Returns the factorial of a number x up to 1023."""
        if num == 0:
            return 1
        elif num < 0:
            return "Error"
        else:
            x = num * functions.fact(num -1)
            return x

    def exp(x):
        """Gives the value of e to the power of the number x."""
        i = 0
        ans = 0
        for i in range(1000):
            ans += (x**i)/functions.fact(i)
        return ans

    def sin(x):
        """Gives sine of the input x, where x is in radians."""
        sinx = 0
        i = 1
        sign = 1
        if x > functions.pi():
            while x > functions.pi():
                x -= 2*functions.pi()
        elif x < -1 * functions.pi():
            while x < -1 * functions.pi():
                x += 2*functions.pi()
        for j in range(50):
            sinx += (sign * (x**i))/functions.fact(i)
            i +=2
            sign *= -1
        sinx = round(sinx, 12)
        return sinx

    def cos(x):
        """Gives cosine of the input x, where x is in radians."""
        cosx = 0
        i = 0
        sign = 1
        if x > functions.pi():
            while x > functions.pi():
                x -= 2*functions.pi()
        elif x < -1 * functions.pi():
            while x < -1 * functions.pi():
                x += 2*functions.pi()
        for j in range(50):
            cosx += (sign * (x**i))/functions.fact(i)
            i +=2
            sign *= -1
        cosx = round(cosx, 12)
        return cosx

    def tan(x):
        """Gives the tangent of the input x, where x is in radians."""
        if functions.cos(x) == 0:
            return "error"
        else:
            tanx = functions.sin(x) / functions.cos(x)
            return tanx

    def ln(x):
        """Returns the natural log of the number x, where x is non negative."""
        if x <= 1:
            x -= 1 
            lnx = 0
            i = 1
            sign = 1
            for j in range(100000):
                lnx += (sign * (x**i))/i
                i += 1
                sign *= -1
            lnx = round(lnx, 12)
            return lnx
        else:
            x = x**(-1)
            lnx = -1 * functions.ln(x)
            return lnx
            

    def sinh(x):
        """Gives the hyperbolic sine of the value x."""
        sinhx = (functions.exp(x) - functions.exp(-x))/2
        return sinhx

    def cosh(x):
        """Gives the hyperbolic cosine of the value x."""
        coshx = (functions.exp(x) + functions.exp(-x))/2
        return coshx

    def tanh(x):
        """Gives the hyperbolic tangent of the value x."""
        tanhx = (functions.exp(2*x) - 1)/(functions.exp(2*x) + 1)
        return tanhx

    def arcsin(x):
        pass

    def arccos(x):
        pass

    def arctan(x):
        pass

    def sqrt(x):
        pass


class opperations(object):
    def __init__(self):
        pass

    def change_sign(x):
        """Changes the sign of the number x."""
        x *= -1
        return x

    def percent(x):
        """Changes number x into a value for percentage - e.g. input 5 --> 0.05."""
        x = x *0.01
        return x

    def div(x, y):
        """Divides number x by number y - y should be non zero."""
        ans = x/y
        return ans

    def mul(x, y):
        """Multiplies number x by number y."""
        ans = x * y
        return ans

    def add(x, y):
        """Adds the number x to the number y."""
        ans = x + y
        return ans

    def sub(x, y):
        """Subtracts the number y from the number x."""
        ans = x - y
        return ans

class calculator(object):
    def __init__(self, num_1 = "0", num_2 = "0", process = "", number_text = ""):
        self.num_1 = num_1
        self.num_2 = num_2
        self.active = [num_1, num_2]
        self.process = process
        self.number_text = number_text
        
    def main(self):
        for i in range(len(all_buttons)):
            x = all_buttons[i].find_clicked()
            if x != None:
                if self.active[0] == "Error" and x in ["1", "2", "3", "4", "5", "6", "7", "8", "9", "0", "."]:
                    if x == ".":
                        self.active[0] = "0" + x
                    else:
                        self.active[0] = x
                elif  x in ["1", "2", "3", "4", "5", "6", "7", "8", "9", "0", "."] and self.number_text.get_width() < 280:
                    if x == "." and "." not in self.active[0]:
                        self.active[0] = self.active[0] + x
                    elif x in ["1", "2", "3", "4", "5", "6", "7", "8", "9", "0"]:
                        if len(self.active[0]) >= 1 and self.active[0] == "0":
                            self.active[0] = x
                        else:
                            self.active[0] = self.active[0] + x
                elif x == "AC":
                    calculator.AC(self)
                elif x == "+/-":
                    calculator.change_sign(self)
                elif x == "%":
                    calculator.percent(self)
        calculator.draw(self)
                    
                    

    def draw(self):
        win.fill((14, 12, 14))
        pygame.draw.rect(win, (88, 88, 88), (0, 0, 320, 40))
        for i in range(len(all_buttons)):
            all_buttons[i].draw()
        self.number_text = font.render(str(self.active[0]), 1, (255, 255, 255))
        win.blit(self.number_text, (310 - (self.number_text.get_width()), 70))
        pygame.display.update()        

    def AC(self):
        """All clear"""
        self.num_1 = "0"
        self.num_2 = "0"
        self.active = [self.num_1, self.num_2]
        self.process = ""

    def change_sign(self):
        self.active[0] = str(-1* float(self.active[0]))
        if float(self.active[0]) == int(float(self.active[0])):
            self.active[0] = str(int(float(self.active[0])))

    def percent(self):
        self.active[0] = str(round(float(self.active[0])/100, 20))


calc = calculator()

    
while True:                             #main loop that uses the FPS to determin the speed so it is the sameon every computer
    for event in pygame.event.get():
        if event.type == QUIT:
            pygame.quit()
            sys.exit()
        calc.main()

