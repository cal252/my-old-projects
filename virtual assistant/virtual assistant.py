import os, sys, time, webbrowser

from urllib.request import urlopen as u_req
from bs4 import BeautifulSoup as soup

class assistant():
	def __init__(self):
		self.name = "assistant"
		self.users_name = "Callum"
		current_hour = time.localtime()[3]
		if current_hour >= 12:
			print("Good afternoon {0}!".format(self.users_name))
		else:
			print("Good morning {0}!".format(self.users_name))

	def get_command(self):
		#command = "show me events"
		command = input("Awaiting command: ")
		if command.lower() == "quit":
			sys.exit()

		elif command[:11] == "add website":
			assistant.get_website_data(self)
			to_add = True
			adding = input("Please enter the website: ")
			adding_url = input("Please enter the url: ")
			#adding = "gmail"
			#adding_url = "https://www.googlemail.com"
			for i in range(len(self.website_list)):
				if adding in self.website_list[i]:
					print("Sorry, that website is already in the list.")
					to_add = False
					break
			if to_add == True:
				assistant.add_url(self, adding, adding_url)
				print("Site has been successfully added.")

		elif command[:5] == "open ":
			assistant.get_website_data(self)
			url = assistant.get_url(self, command[5:])
			assistant.open_website(self, url)

		elif "what" in command.lower() and "time" in command.lower():
			assistant.get_time(self)
		
		elif "what" in command.lower() and "date" in command.lower():
			assistant.get_date(self)

		elif ("what" in command.lower() or "tell" in command.lower()) and "weather" in command.lower():
			conditions = assistant.get_weather(self)
			temps = assistant.get_temps(self)
			specific = False
			for i in range(len(days)):
				if days[i] in command.lower():
					assistant.tell_weather(self, days[i], temps, conditions)
					specific = True
					break
			if specific == False:
				assistant.tell_weather(self, "today", temps, conditions)

		elif "add" in command.lower() and "event" in command.lower():
			assistant.add_event(self)

		elif ("show" in command.lower() or "tell" in command.lower() or "what" in command.lower()) and "event" in command.lower():
			assistant.get_events(self)

		elif command == "help":
			print("""
				add website - to add a website to the list to be opened
				open ... - to open a website in the list
				ask me what the time is - for me to tell you the time
				ask me what the date is - for me to tell you the date
				ask me what the weather is like - for me to tell you the weather
				quit - to exit""")

		else:
			print("I did not recognise that command, please tell me another.\n")

	def get_website_data(self):
		file = open("websites.txt", "r")
		websites = []
		temp_websites = file.readlines()
		for i in range(len(temp_websites)):
			websites.append(temp_websites[i].split(",")[:2])
		self.website_list = websites
		file.close()

	def get_url(self, website):
		url = None
		for i in range(len(self.website_list)):
			if website == self.website_list[i][0]:
				url = self.website_list[i][1]
				break
		return url

	def open_website(self, url):
		webbrowser.open(url)

	def add_url(self, website, url):
		file = open("websites.txt", "a")
		file.write(website + "," + url + ",\n")
		file.close()

	def get_time(self):
		current_time = [str(time.localtime()[3]), str(time.localtime()[4])]
		if int(current_time[0]) >= 12:
			time_stamp = "pm"
		else:
			time_stamp = "am"
		if int(current_time[0]) > 12:
			current_time[0] = str(int(current_time[0]) - 12)
		if int(current_time[1]) < 10:
			current_time[1] = "0" + current_time[1]
		
		print("The current time is {0}:{1} {2}.".format(current_time[0], current_time[1], time_stamp))

	def get_date(self):
		current_date = [str(time.localtime()[2]), str(time.localtime()[1]), str(time.localtime()[0])]
		if current_date[0] == "11" or current_date[0] == "12" or current_date[0] == "13":
			suffix = "th"
		else:
			try:
				suffix = date_dict[current_date[0][-1]]
			except:
				suffix = "th"
		print("The date is {0}{1} of {2} {3}.".format(current_date[0], suffix, month_dict[current_date[1]], current_date[2]))

	def get_weather(self):
		weather_url = 'https://www.bbc.co.uk/weather/2633771' 
		u_client = u_req(weather_url)
		page_html = u_client.read()
		u_client.close()

		page_soup = soup(page_html, "html.parser")

		weather_conditions = page_soup.findAll("div", {"class":"wr-day__details__weather-type-description"})

		for i in range(len(weather_conditions)):
			weather_conditions[i] = str(weather_conditions[i])[55:-6]

		weather_conditions = weather_conditions[:8]
		return weather_conditions

	def get_temps(self):
		weather_url = 'https://www.bbc.co.uk/weather/2633771'
		u_client = u_req(weather_url)
		page_html = u_client.read()
		u_client.close()

		page_soup = soup(page_html, "html.parser")
		temps = page_soup.findAll("span", {"class":"wr-value--temperature--c"})
		temps = temps[:16]
		highs = []
		lows = []
		for i in range(len(temps)):
			temps[i] = str(temps[i])[39:-7]
			if i % 2 == 0:
				highs.append(temps[i])
			else:
				lows.append(temps[i])

		if highs[0] < lows[0]:
			temp_highs = highs
			highs = lows
			lows = temp_highs
			highs.insert(0, "n/a")
			del highs[8]

		#print(temps)
		#print(highs)
		#print(lows)

		return highs, lows

	def tell_weather(self, day, temps, conditions):
		highs = temps[0]
		lows = temps[1]
		week_day = time.localtime()[6]
		if day == "today":
			print("Today the weather forcast is {0}, with highs of {1} and lows of {2}.".format(conditions[0], highs[0], lows[0]))
		else:

			asking = days_dict[day]
			index = asking - week_day
			if index < 0:
				index -= 1
			if index == 0:
				index = 7
			print("The weather forcast for {0} is {1}, with highs of {2} and lows of {3}.".format(day, conditions[index], highs[index], lows[index]))

	def add_event(self):
		file = open("events.txt", "a")
		validated = False
		while validated == False:
			print("Type \"exit\" to go back")
			event = input("Enter the event: ")
			if event.lower() == "exit":
				break
			day = input("Please enter the day of the event (number): ")
			if day.lower() == "exit":
				break
			month = input("Please enter the month of the event (number): ")
			if month.lower() == "exit":
				break
			yearly = input("If this event is yearly type \"1\", otherwise enter the year it is for: ")
			if yearly.lower() == "exit":
				break
			if len(event) >= 6 and int(month) > 0 and int(month) < 13 and len(day) > 0 and len(yearly) > 0:

				if day == "11" or day == "12" or day == "13":
					suffix = "th"
				else:
					try:
						suffix = date_dict[day[-1]]
					except:
						suffix = "th"

				file.write("{0},{1},{2},{3},{4},\n".format(event, day, month, yearly, suffix))
				validated = True
			else:
				print("You have made a mistake while entering data, please enter it again.")
		print("")
		file.close()

	def get_events(self):
		file = open("events.txt", "r")
		events = []
		temp_events = file.readlines()
		for i in range(len(temp_events)):
			events.append(temp_events[i].split(",")[:5])
		file.close()
		current_date = [time.localtime()[2], time.localtime()[1], time.localtime()[0]] #day month year
		relavent_events_year = []
		for i in range(len(events)):
			if events[i][3] == "1" or events[i][3] == str(current_date[2]):
				relavent_events_year.append(events[i])

		this_month_events = []
		next_month_events = []
		for i in range(len(relavent_events_year)):
			if int(current_date[0]) <= 7:
				if int(relavent_events_year[i][1]) >= current_date[0]:
					if int(relavent_events_year[i][2]) == current_date[1]:
						this_month_events.append(relavent_events_year[i])
			else:
				if int(relavent_events_year[i][2]) == current_date[1]:
					if int(relavent_events_year[i][1]) >= current_date[0]:
						this_month_events.append(relavent_events_year[i])
				elif int(relavent_events_year[i][2]) == (current_date[1] + 1):
					next_month_events.append(relavent_events_year[i])

		this_month_events = sorted(this_month_events, key = lambda x:x[1])
		next_month_events = sorted(next_month_events, key = lambda x:x[1])

		if len(this_month_events) == 0 :
			print("No events for this month.")
			print("")
		else:
			print("Events this month:")
			for i in range(len(this_month_events)):
				if this_month_events[i][1] == str(current_date[0]):
					print(">{0} - TODAY!".format(this_month_events[i][0]))
				else:
					print(">{0} - {1}{2} of {3}.".format(this_month_events[i][0], this_month_events[i][1], this_month_events[i][4], month_dict[this_month_events[i][2]]))
			print("")
		if len(next_month_events) > 0:
			print("Events next month:")
			for i in range(len(next_month_events)):
				print(">{0} - {1}{2} of {3}.".format(next_month_events[i][0], next_month_events[i][1], next_month_events[i][4], month_dict[next_month_events[i][2]]))
			print("")


days = ["monday", "tuesday", "wednesday", "thursday", "friday", "saturday", "sunday"]
days_dict = {"monday":0, "tuesday":1, "wednesday":2, "thursday":3, "friday":4, "saturday":5, "sunday":6}
date_dict = {"1":"st", "2":"nd", "3":"rd"}
month_dict = {"1":"January", "2":"February", "3":"March", "4":"April", "5":"May", "6":"June", "7":"July", "8":"August", "9":"September", "10":"October", "11":"November", "12":"December"}


assistant = assistant()


while True:
	assistant.get_command()
