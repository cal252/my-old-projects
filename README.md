# My old projects

This is a repository holding some of my old projects that I created when I first started learning to code (all the projects that are resonably big and not just small scripts). A lot of these are quite bad so please don't judge 😁

# General info

Each project has its own directory with a markdown file, this will have a little more information about each mini project as well as the date it was last modified.


## License
[BSD 3-Clause License](LICENSE)
