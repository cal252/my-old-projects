#NEA
#requires adding to a txt file and '#' comments for the school one.

#in format:r1[[couple name, [min, score 2, score 3, score 4, max],r1 min+max, r1 total,
#r2[min, score 2, score 3, score 4, max], r2 min+max, r2 total, r1+2total,
#fr[min, score 2, score 3, score 4, max], fr min+max, fr total, r1+2+f total]...]
couple_scores = [["Couple A"],
                 ["Couple B"],
                 ["Couple C"],
                 ["Couple D"],
                 ["Couple E"],
                 ["Couple F"]]
removed_couples = []
print("-"*80)
print(" "*34, "Dancers UK")
print("-"*80)
print(" "*35, "Round  1")
print("-"*80)
validation = False
while validation == False:
    for x in range(len(couple_scores)):
        correct_scores = False
        while correct_scores == False:
            print("please enter the score for " + couple_scores[x][0])
            judge_1 = int(input("judge 1: "))
            judge_2 = int(input("judge 2: "))
            judge_3 = int(input("judge 3: "))
            judge_4 = int(input("judge 4: "))
            judge_5 = int(input("judge 5: "))
            scores = [judge_1, judge_2, judge_3, judge_4, judge_5]
            if judge_1 >=1 and judge_1 <=10 and judge_2 >=1 and judge_2 <=10 and judge_3 >=1 and judge_3 <=10 and judge_4 >=1 and judge_4 <=10 and judge_5 >=1 and judge_5 <=10:
                correct_scores = True
                scores.sort()
                couple_scores[x].append(scores)
                min_max = scores[0] + scores[4]
                couple_scores[x].append(min_max)
                total = sum(scores[1:4])
                couple_scores[x].append(total)
                print(couple_scores[x][0], "scored", couple_scores[x][3], "in round 1")
            else:
                print("some of the score(s) where not correct, please ensure all scores are from 1-10")
                correct_scores = False
                
    correct_answer = False
    while correct_answer == False:
        answer = input("is all of the inputted data above correct? Y/N? ").upper()
        if answer == "Y":
            validation = True
            correct_answer = True
        elif answer == "N":
            print("ok, please enter the scores again")
            validation = False
            correct_answer = True
        else:
            correct_answer = False
couple_totals_accending = sorted(couple_scores, key = lambda x:x[3])
print(couple_totals_accending)
if couple_totals_accending[1][3] == couple_totals_accending[2][3]:
    print(couple_totals_accending[1][0], "got the same score as", couple_totals_accending[2][0], "the minimum and maximum will be used to decide who goes through")
    if couple_totals_accending[1][2] > couple_totals_accending[2][2]:
        new = []
        temp_couple = couple_totals_accending
        new.append(temp_couple[0])
        new.append(temp_couple[2])
        new.append(temp_couple[1])
        new.append(temp_couple[3])
        new.append(temp_couple[4])
        new.append(temp_couple[5])
        couple_totals_accending = new
    elif couple_totals_accending[1][2] == couple_totals_accending[2][2]:
        if couple_totals_accending[1][1][4] > couple_totals_accending[2][1][4]:
            new = []
            temp_couple = couple_totals_accending
            new.append(temp_couple[0])
            new.append(temp_couple[2])
            new.append(temp_couple[1])
            new.append(temp_couple[3])      #needs carrying on and completing if needed
            new.append(temp_couple[4])      #/have time
            new.append(temp_couple[5])
            couple_totals_accending = new
        
removed_couples.append(couple_totals_accending[0])
removed_couples.append(couple_totals_accending[1])
print(removed_couples)
found = False
for n in range(len(couple_scores)):
    if found == False:
        if couple_scores[n][0] == removed_couples[0][0]:
            print(couple_scores[n][0], "has beem eliminated")
            del(couple_scores[n])
            found = True
found = False
for n in range(len(couple_scores)):
    if found == False:
        if couple_scores[n][0] == removed_couples[1][0]:
            print(couple_scores[n][0], "has been eliminated")
            del(couple_scores[n])
            found = True
print(couple_scores)
#round 2
print("-"*80)
print(" "*35, "Round  2")
print("-"*80)
validation = False
while validation == False:
    for x in range(len(couple_scores)):
        correct_scores = False
        while correct_scores == False:
            print("please enter the score for " + couple_scores[x][0])
            judge_1 = int(input("judge 1: "))
            judge_2 = int(input("judge 2: "))
            judge_3 = int(input("judge 3: "))
            judge_4 = int(input("judge 4: "))
            judge_5 = int(input("judge 5: "))
            scores_2 = [judge_1, judge_2, judge_3, judge_4, judge_5]
            if judge_1 >=1 and judge_1 <=10 and judge_2 >=1 and judge_2 <=10 and judge_3 >=1 and judge_3 <=10 and judge_4 >=1 and judge_4 <=10 and judge_5 >=1 and judge_5 <=10:
                correct_scores = True
                scores_2.sort()
                couple_scores[x].append(scores_2)
                min_max_2 = scores_2[0] + scores_2[4]
                couple_scores[x].append(min_max_2)
                total_2 = sum(scores_2[1:4])
                couple_scores[x].append(total_2)
                multi_round_total = couple_scores[x][3] + couple_scores[x][6]
                couple_scores[x].append(multi_round_total)
                print(couple_scores[x][0], "scored", couple_scores[x][6], "in round 2, there complete total is", couple_scores[x][7])
            else:
                print("some of the score(s) where not correct, please ensure all scores are from 1-10")
                correct_scores = False
                
    correct_answer = False
    while correct_answer == False:
        answer = input("is all of the inputted data above correct? Y/N? ").upper()
        if answer == "Y":
            validation = True
            correct_answer = True
        elif answer == "N":
            print("ok, please enter the scores again")
            validation = False
            correct_answer = True
        else:
            correct_answer = False
couple_totals_accending_2 = sorted(couple_scores, key = lambda x:x[7])
print(couple_totals_accending_2)
if couple_totals_accending_2[1][7] == couple_totals_accending_2[2][7]:
    print(couple_totals_accending_2[1][0], "got the same score as", couple_totals_accending_2[2][0], "the minimum and maximum will be used to decide who goes through")
    if couple_totals_accending_2[1][5] > couple_totals_accending_2[2][5]:
        new = []
        temp_couple = couple_totals_accending_2
        new.append(temp_couple[0])
        new.append(temp_couple[2])
        new.append(temp_couple[1])
        new.append(temp_couple[3])
        couple_totals_accending_2 = new
    elif couple_totals_accending_2[1][5] == couple_totals_accending_2[2][5]:
        if couple_totals_accending_2[1][6] > couple_totals_accending_2[2][6]:
            new = []
            temp_couple = couple_totals_accending_2
            new.append(temp_couple[0])
            new.append(temp_couple[2])
            new.append(temp_couple[1])
            new.append(temp_couple[3])         #needs carrying on and completing if needed
            couple_totals_accending_2 = new    #/have time
           
removed_couples.append(couple_totals_accending_2[0])
removed_couples.append(couple_totals_accending_2[1])
print(removed_couples)
found = False
for n in range(len(couple_scores)):
    if found == False:
        if couple_scores[n][0] == removed_couples[2][0]:
            print(couple_scores[n][0], "has beem eliminated")
            del(couple_scores[n])
            found = True
found = False
for n in range(len(couple_scores)):
    if found == False:
        if couple_scores[n][0] == removed_couples[3][0]:
            print(couple_scores[n][0], "has been eliminated")
            del(couple_scores[n])
            found = True
print(couple_scores)
#final round
print("-"*80)
print(" "*33, "Final Round")
print("-"*80)
validation = False
while validation == False:
    for x in range(len(couple_scores)):
        correct_scores = False
        while correct_scores == False:
            print("please enter the score for " + couple_scores[x][0])
            judge_1 = int(input("judge 1: "))
            judge_2 = int(input("judge 2: "))
            judge_3 = int(input("judge 3: "))
            judge_4 = int(input("judge 4: "))
            judge_5 = int(input("judge 5: "))
            scores_f = [judge_1, judge_2, judge_3, judge_4, judge_5]
            if judge_1 >=1 and judge_1 <=10 and judge_2 >=1 and judge_2 <=10 and judge_3 >=1 and judge_3 <=10 and judge_4 >=1 and judge_4 <=10 and judge_5 >=1 and judge_5 <=10:
                correct_scores = True
                scores_f.sort()
                couple_scores[x].append(scores_f)
                min_max_f = scores_f[0] + scores_f[4]
                couple_scores[x].append(min_max_f)
                total_f = sum(scores_f[1:4])
                couple_scores[x].append(total_f)
                multi_round_total_f = couple_scores[x][3] + couple_scores[x][6] + couple_scores[x][10]
                couple_scores[x].append(multi_round_total_f)
                print(couple_scores[x][0], "scored", couple_scores[x][10], "in the final round, there complete total is", couple_scores[x][11])
            else:
                print("some of the score(s) where not correct, please ensure all scores are from 1-10")
                correct_scores = False
                
    correct_answer = False
    while correct_answer == False:
        answer = input("is all of the inputted data above correct? Y/N? ").upper()
        if answer == "Y":
            validation = True
            correct_answer = True
        elif answer == "N":
            print("ok, please enter the scores again")
            validation = False
            correct_answer = True
        else:
            correct_answer = False
couple_totals_accending_f = sorted(couple_scores, key = lambda x:x[11])
print(couple_totals_accending_f)
if couple_totals_accending_f[0][11] == couple_totals_accending_f[1][11]:
    print(couple_totals_accending_f[0][0], "got the same score as", couple_totals_accending_f[1][0], "the minimum and maximum will be used to decide who wins!")
    if couple_totals_accending_f[0][9] > couple_totals_accending_f[1][9]:
        new = []
        temp_couple = couple_totals_accending_f
        new.append(temp_couple[1])
        new.append(temp_couple[0])
        couple_totals_accending_f = new
    elif couple_totals_accending_f[0][9] == couple_totals_accending_f[1][9]:
        if couple_totals_accending_f[0][10] > couple_totals_accending_f[1][10]:
            new = []
            temp_couple = couple_totals_accending_2
            new.append(temp_couple[1])
            new.append(temp_couple[0])
            couple_totals_accending_f = new
        
removed_couples.append(couple_totals_accending_f[0])
print(removed_couples)
found = False
for n in range(len(couple_scores)):
    if found == False:
        if couple_scores[n][0] == removed_couples[4][0]:
            print(couple_scores[n][0], "has beem eliminated")
            del(couple_scores[n])
            found = True
print(couple_scores)
print(couple_scores[0][0], "wins!")
