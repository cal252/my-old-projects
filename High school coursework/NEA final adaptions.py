#NEA

#in format:r1[[couple name, [unsorted scores], [min score 2, score 3, scsore 4, max],r1 min+max, r1 total,
#r2[unsorted scores], [min, score 2, score 3, score 4, max], r2 min+max, r2 total, r1+r2 total,
#fr[unsorted scores], [min,score 2, score 3, score 4, max], fr min+max, fr total, r1+r2+fr total]...]
couple_scores = [["Couple A"],
                 ["Couple B"],
                 ["Couple C"],
                 ["Couple D"],
                 ["Couple E"],
                 ["Couple F"]]
removed_couples = []

def new_round(Round, no_spaces):
    print("-"*80)
    print(" "*no_spaces, Round)
    print("-"*80)

def elimination(v, q):
    removed_couples.append(v[q])
    print(removed_couples)
    found = False
    for n in range(len(couple_scores)):
        if found == False:
            if couple_scores[n][0] == removed_couples[-1][0]:
                print(couple_scores[n][0], "has been eliminated")
                del(couple_scores[n])
                found = True

def add_file(name, name_no, round_list):
    stats.write(name[name_no][0])
    stats.write("\n")
    stats.write("Judge 1    Judge 2    Judge 3    Judge 4    Judge 5")
    stats.write("\n")
    stats.write(" "*3)
    stats.write(str(name[name_no][round_list][0]))
    stats.write(" "*(11-len(str(name[name_no][round_list][0]))))
    stats.write(str(name[name_no][round_list][1]))
    stats.write(" "*(11-len(str(name[name_no][round_list][1]))))
    stats.write(str(name[name_no][round_list][2]))
    stats.write(" "*(11-len(str(name[name_no][round_list][2]))))
    stats.write(str(name[name_no][round_list][3]))
    stats.write(" "*(11-len(str(name[name_no][round_list][3]))))
    stats.write(str(name[name_no][round_list][4]))
    stats.write("\n\n")
    
#round 1
print("-"*80)
print(" "*34, "Dancers UK")
new_round("Round 1", 35)
validation = False
while validation == False:
    for x in range(len(couple_scores)):
        correct_scores = False
        while correct_scores == False:
            print("Please enter the score for " + couple_scores[x][0])
            judge_1 = int(input("Judge 1: "))
            judge_2 = int(input("Judge 2: "))
            judge_3 = int(input("Judge 3: "))
            judge_4 = int(input("Judge 4: "))
            judge_5 = int(input("Judge 5: "))
            unsorted_scores = [judge_1, judge_2, judge_3, judge_4, judge_5] 
            scores = [judge_1, judge_2, judge_3, judge_4, judge_5]
            if judge_1 >= 1 and judge_1 <=10 and judge_2 >=1 and judge_2 <=10 and judge_3 >=1 and judge_3 <=10 and judge_4 >=1 and judge_4 <=10 and judge_5 >=1 and judge_5 <=10:
                correct_scores = True
                couple_scores[x].append(unsorted_scores)
                scores.sort()
                couple_scores[x].append(scores)
                min_max = scores[0] + scores[4]
                couple_scores[x].append(min_max)
                total = sum(scores[1:4])
                couple_scores[x].append(total)
                print(couple_scores[x][0], "scored", couple_scores[x][4], "in round 1")
            else:
                print("Some of the score(s) where not correct, please ensure all scores are from 1-10")
                correct_scores = False

    correct_answer = False
    while correct_answer == False:
        answer = input("Is all of the inputted data above correct? Y/N? ").upper()
        if answer == "Y":
            validation = True
            correct_answer = True
        elif answer == "N":
            print("Ok, please enter the scores again")
            validation = False
            correct_answer = True
            for y in range(len(couple_scores)):
                del(couple_scores[y][4])
                del(couple_scores[y][3])
                del(couple_scores[y][2])
                del(couple_scores[y][1])
        else:
            correct_answer = False
            
couple_totals_accending = sorted(couple_scores, key = lambda x:x[4])
print(couple_totals_accending)
if couple_totals_accending[1][4] == couple_totals_accending[2][4]:
    print(couple_totals_accending[1][0], "got the same score as", couple_totals_accending[2][0], "the minimum and maximum will be used to decide who goes through")
    if couple_totals_accending[1][3] > couple_totals_accending[2][3]:
        new = []
        temp_couple = couple_totals_accending
        new.append(temp_couple[0])
        new.append(temp_couple[2])
        new.append(temp_couple[1])
        new.append(temp_couple[3])
        new.append(temp_couple[4])
        new.append(temp_couple[5])
        couple_totals_accending = new
    elif couple_totals_accending[1][3] == couple_totals_accending[2][3]:
        if couple_totals_accending[1][2][4] > couple_totals_accending[2][2][4]:
            new = []
            temp_couple = couple_totals_accending
            new.append(temp_couple[0])
            new.append(temp_couple[2])
            new.append(temp_couple[1])
            new.append(temp_couple[3])
            new.append(temp_couple[4])
            new.append(temp_couple[5])
            couple_totals_accending = new

elimination(couple_totals_accending, 0)
elimination(couple_totals_accending, 1)

print(couple_scores)
#round 2
new_round("Round 2", 35)
validation = False
while validation == False:
    for x in range(len(couple_scores)):
        correct_scores = False
        while correct_scores == False:
            print("Please enter the score for " + couple_scores[x][0])
            judge_1 = int(input("Judge 1: "))
            judge_2 = int(input("Judge 2: "))
            judge_3 = int(input("Judge 3: "))
            judge_4 = int(input("Judge 4: "))
            judge_5 = int(input("Judge 5: "))
            unsorted_scores_2 = [judge_1, judge_2, judge_3, judge_4, judge_5]
            scores_2 = [judge_1, judge_2, judge_3, judge_4, judge_5]
            if judge_1 >=1 and judge_1 <=10 and judge_2 >=1 and judge_2 <=10 and judge_3 >=1 and judge_3 <=10 and judge_4 >=1 and judge_4 <=10 and judge_5 >=1 and judge_5 <=10:
                correct_scores = True
                couple_scores[x].append(unsorted_scores_2)
                scores_2.sort()
                couple_scores[x].append(scores_2)
                min_max_2 = scores_2[0] + scores_2[4]
                couple_scores[x].append(min_max_2)
                total_2 = sum(scores_2[1:4])
                couple_scores[x].append(total_2)
                multi_round_total = couple_scores[x][4] + couple_scores[x][8]
                couple_scores[x].append(multi_round_total)
                print(couple_scores[x][0], "scored", couple_scores[x][8], "in round 2, there complete total is", couple_scores[x][9])
            else:
                print("Some of the score(s) where not correct, please ensure all scores are from 1_10")
                correect_scores = False
                
    correct_answer = False
    while correct_answer == False:
        answer = input("Is all of the inputted data above correct? Y/N? ").upper()
        if answer == "Y":
            validation = True
            correct_answer = True
        elif answer == "N":
            print("Ok, please enter the scores again")
            validation = False
            correct_answer = True
            for y in range(len(couple_scores)):
                del(couple_scores[y][9])
                del(couple_scores[y][8])
                del(couple_scores[y][7])
                del(couple_scores[y][6])
                del(couple_scores[y][5])
            else:
                correct_answer = False
                
couple_totals_accending_2 = sorted(couple_scores, key = lambda x:x[9])
print(couple_totals_accending_2)
if couple_totals_accending_2[1][9] == couple_totals_accending[2][9]:
    print(couple_totals_accending_2[1][0], "got the same score as", couple_totals_accending_2[2][0], "the minimum and maximum will be used to decide who goes through")
    if couple_totals_accending_2[1][7] > couple_totals_accending_2[2][7]:
        new = []
        temp_couple = couple_totals_accending_2
        new.append(temp_couple[0])
        new.append(temp_couple[2])
        new.append(temp_couple[1])
        new.append(temp_couple[3])
        couple_totals_accending_2 = new
    elif couple_totals_accending_2[1][7] == couple_totals_accending_2[2][7]:
        if couple_totals_accending_2[1][8] > couple_totals_accending[2][8]:
            new = []
            temp_couple = couple_totals_accending_2
            new.append(temp_couple[0])
            new.append(temp_couple[2])
            new.append(temp_couple[1])
            new.append(temp_couple[3])
            couple_totals_accending_2 = new

elimination(couple_totals_accending_2, 0)
elimination(couple_totals_accending_2, 1)

print(couple_scores)
#final round
new_round("Final Round", 33)
validation = False
while validation == False:
    for x in range(len(couple_scores)):
        correct_scores = False
        while correct_scores == False:
            print("Please enter the score for " + couple_scores[x][0])
            judge_1 = int(input("Judge 1: "))
            judge_2 = int(input("Judge 2: "))
            judge_3 = int(input("Judge 3: "))
            judge_4 = int(input("judge 4: "))
            judge_5 = int(input("Judge 5: "))
            unsorted_scores_f = [judge_1, judge_2, judge_3, judge_4, judge_5]
            scores_f = [judge_1, judge_2, judge_3, judge_4, judge_5]
            if judge_1 >= 1 and judge_1 <= 10 and judge_2 >= 1 and judge_2 <= 10 and judge_3 >= 1 and judge_3 <= 10 and judge_4 >= 1 and judge_4 <= 10 and judge_5>= 1 and judge_5 <= 10:
                correct_scores = True
                couple_scores[x].append(unsorted_scores_f)
                scores_f.sort()
                couple_scores[x].append(scores_f)
                min_max_f = scores_f[0] + scores_f[4]
                couple_scores[x].append(min_max_f)
                total_f = sum(scores_f[1:4])
                couple_scores[x].append(total_f)
                multi_round_total_f = couple_scores[x][4] + couple_scores[x][8] + couple_scores[x][13]
                couple_scores[x].append(multi_round_total_f)
                print(couple_scores[x][0], "scored", couple_scores[x][13], "in the final round, there complete total is", couple_scores[x][14])
            else:
                print("Some of the score(s) where not corrrect, please ensure all the scores are from 1-10")
                correct_scores = False
   
    correct_answer = False
    while correct_answer == False:
        answer = input("is all of the inputted data above correct? Y/N? ").upper()
        if answer == "Y":
            validation = True
            correct_answer = True
        elif answer == "N":
            print("ok, please enter the scores again")
            validation = False
            correct_answer = True
            for y in range(len(couple_scores)):
                del(couple_scores[y][10])
                del(couple_scores[y][11])
                del(couple_scores[y][12])
                del(couple_scores[y][13])
                del(couple_scores[y][14])
        else:
            correct_answer = False

couple_totals_accending_f = sorted(couple_scores, key = lambda x:x[14])
print(couple_totals_accending_f)
if couple_totals_accending_f[0][14] == couple_totals_accending_f[1][14]:
    print(couple_totals_accending_f[0][0], "got the same as", couple_totals_accending_f[1][0], "the minimum and maximum will be used to decide who wins!")
    if couple_totals_accending_f[0][12] > couple_totals_accending_f[1][12]:
        new = []
        temp_couple = couple_totals_accending_f
        new.append(temp_couple[1])
        new.append(temp_couple[0])
        couple_totals_accending_f = new
    elif couple_totals_accending_f[0][12] == couple_totals_accending_f[1][12]:
        if couple_totals_accending_f[0][13] > couple_totals_accending_f[1][13]:
            new = []
            temp_couple = couple_totals_accending_f
            new.append(temp_couple[1])
            new.append(temp_couple[0])
            couple_totals_accending_f = new

elimination(couple_totals_accending_f, 0)

print(couple_scores)
print(couple_scores[0][0], "wins!")

stats = open("statistics.txt", "w")

stats.write(" "*22 + "Round 1")
stats.write("\n\n")
add_file(couple_scores, 0, 1)
add_file(removed_couples, 4, 1)
add_file(removed_couples, 3, 1)
add_file(removed_couples, 2, 1)
add_file(removed_couples, 1, 1)
add_file(removed_couples, 0, 1)

stats.write("\n")
stats.write(" "*22 + "Round 2")
stats.write("\n\n")
add_file(couple_scores, 0, 5)
add_file(removed_couples, 4, 5)
add_file(removed_couples, 3, 5)
add_file(removed_couples, 2, 5)


stats.write("\n")
stats.write(" "*20 + "Final Round")
stats.write("\n\n")
add_file(couple_scores, 0, 10)
add_file(removed_couples, 4, 10)
stats.close()
