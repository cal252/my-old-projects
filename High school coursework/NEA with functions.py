#NEA


#in format:r1[[couple name, [min score 2, score 3, scsore 4, max],r1 min+max, r1 total, r1+_total
#r2[min, score 2, score 3, score 4, max], r2 min+max, r2 total, r1+r2 total,
#fr[min,score 2, score 3, score 4, max], fr min+max, fr total, r1+r2+fr total]...]

couple_scores = [["Couple A"],
                 ["Couple B"],
                 ["Couple C"],
                 ["Couple D"],
                 ["Couple E"],
                 ["Couple F"]]
removed_couples = []


def new_round(Round, no_spaces):
    print("-"*80)
    print(" "*no_spaces, Round)
    print("-"*80)

def elimination(v, q):
    removed_couples.append(v[q])
    print(removed_couples)
    found = False
    for n in range(len(couple_scores)):
        if found == False:
            if couple_scores[n][0] == removed_couples[-1][0]:
                print(couple_scores[n][0], "has been eliminated")
                del(couple_scores[n])
                found = True
    
print("-"*80)
print(" "*34, "Dancers UK")
new_round("Round 1", 35)

validation = False
while validation == False:
    for x in range(len(couple_scores)):
        correct_scores = False
        while correct_scores == False:
            print("Please enter the score for " + couple_scores[x][0])
            judge_1 = int(input("Judge 1: "))
            judge_2 = int(input("Judge 2: "))
            judge_3 = int(input("Judge 3: "))
            judge_4 = int(input("Judge 4: "))
            judge_5 = int(input("Judge 5: "))
            scores = [judge_1, judge_2, judge_3, judge_4, judge_5]
            if judge_1 >=1 and judge_1 <=10 and judge_2 >=1 and judge_2 <=10 and judge_3 >=1 and judge_3 <=10 and judge_4 >=1 and judge_4 <=10 and judge_5 >=1 and judge_5 <=10:
                correct_scores = True
                scores.sort()
                couple_scores[x].append(scores)
                min_max = scores[0] + scores[4]
                couple_scores[x].append(min_max)
                total = sum(scores[1:4])
                couple_scores[x].append(total)
                print(couple_scores[x][0], "scored", couple_scores[x][3], "in round 1.")
            else:
                print("Some of the score(s) where not correct, please ensure all scores are from 1_10")

    correct_answer = False
    while correct_answer == False:
        answer = input("Is all of the inputted data above correct? Y/N? ").upper()
        if answer == "Y":
            validation = True
            correct_answer = True
        elif answer == "N":
            print("Ok, please enter the scores again")
            validation = False
            correct_answer = True
            for y in range(len(couple_scores)):
                del(couple_scores[y][3])
                del(couple_scores[y][2])
                del(couple_scores[y][1])
        else:
            correct_answer = False


couple_totals_accending = sorted(couple_scores, key = lambda x:x[3])
print(couple_totals_accending)
if couple_totals_accending[1][3] == couple_totals_accending[2][3]:
    print(couple_totals_accending[1][0], "got the same score as", couple_totals_accending[2][0], "the minimum and maximum will be used to decide who goes through")
    if couple_totals_accending[1][2] > couple_totals_accending[2][2]:
        new = []
        temp_couple = couple_totals_accending
        new.append(temp_couple[0])
        new.append(temp_couple[2])
        new.append(temp_couple[1])
        new.append(temp_couple[3])
        new.append(temp_couple[4])
        new.append(temp_couple[5])
        couple_totals_accending = new
    elif couple_totals_accending[1][2] == couple_totals_accending[2][2]:
        if couple_totals_accending[1][1][4] > couple_totals_accending[2][1][4]:
            new = []
            temp_couple = couple_totals_accending
            new.append(temp_couple[0])
            new.append(temp_couple[2])
            new.append(temp_couple[1])
            new.append(temp_couple[3])
            new.append(temp_couple[4])
            new.append(temp_couple[5])
            couple_totals_accending = new


elimination(couple_totals_accending, 0)
elimination(couple_totals_accending, 1)

print(couple_scores)














                


