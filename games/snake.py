import pygame, sys, random, time
from pygame.locals import *
import random

pygame.init()

FPS = 4

screen_width = 721
screen_height = 721

win = pygame.display.set_mode((screen_width, screen_height), 0, 32)
pygame.display.set_caption("Snake")

class board(object):
	def __init__(self, width, height):
		self.width = width
		self.height = height


	def draw(self):
		win.fill((12, 12, 12))
		for i in range(self.width + 1):
			pygame.draw.line(win, (120, 120, 120), (int(screen_width / self.width) * i, 0), (int(screen_width / self.width) * i, screen_height))
		for j in range(self.height + 1):
			pygame.draw.line(win, (120, 120, 120), (0, int(screen_height / self.height) * j), (screen_width, int(screen_height / self.height) * j))
		pygame.display.update()

class snake(object):
	def __init__(self):
		self.pieces = [[2,0],[1,0],[0,0]]
		self.direction = "d" 

	def move(self):
		if self.direction == "l":
			del self.pieces[-1]
			self.pieces.insert(0, [self.pieces[0][0] + 1, self.pieces[0][1]])
		if self.direction == "r":
			del self.pieces[-1]
			self.pieces.insert(0, [self.pieces[0][0] - 1, self.pieces[0][1]])
		if self.direction == "u":
			del self.pieces[-1]
			self.pieces.insert(0, [self.pieces[0][0], self.pieces[0][1] - 1])
		if self.direction == "d":
			del self.pieces[-1]
			self.pieces.insert(0, [self.pieces[0][0], self.pieces[0][1] + 1])

	def get_direction(self):
		for event in pygame.event.get():
			if event.type == pygame.KEYDOWN:
				if event.key == pygame.K_w:
					self.direction = "u"
				elif event.type == pygame.K_s:
					self.direction = "d"
				elif event.type == pygame.K_a:
					self.direction = "l"
				elif event.type == pygame.K_d:
					self.direction = "r"

	def draw(self):
		for i in range(len(self.pieces)):
			pygame.draw.rect(win, (240, 240, 240), (self.pieces[i][0] * (int(screen_width / main_board.width)) + 3, self.pieces[i][1] * (int(screen_height / main_board.height)) + 3, int(screen_width / main_board.width) - 5, int(screen_height / main_board.height) - 5))
		pygame.display.update()

	def play(self):
		main_board.draw()
		snake.get_direction(self)
		snake.move(self)
		snake.draw(self)

class fruit(object):
	def __init__(self):
		pos = fruit.get_pos(self)
		self.x = pos[0]
		self.y = pos[1]

	def get_pos(self):
		x = random.randint(0, main_board.width)
		y = random.randint(0, main_board.height)
		return (x, y)

	def draw(self):
		pygame.display.update()

		


main_board = board(20,20)
main_board.draw()

main_snake = snake()
main_snake.draw()

clock = pygame.time.Clock()
while True:
    clock.tick(FPS)
    for event in pygame.event.get():
        if event.type == QUIT:
            pygame.quit()
            sys.exit()
    main_snake.play()