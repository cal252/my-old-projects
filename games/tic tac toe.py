import pygame, sys, os, random
from pygame.locals import *

pygame.init()

FPS = 0

screen_width = 800
screen_height = 600

win = pygame.display.set_mode((screen_width, screen_height), 0, 32)
pygame.display.set_caption("Tic Tac Toe")

font1 = pygame.font.SysFont("comicsans", 48, True)
font2 = pygame.font.SysFont("comicsans", 36, True)

class button(object):
    def __init__(self, x, y, width, height, colour, back_colour, text):
        self.x = x                              #x and y co-ordinates of the button
        self.y = y
        self.width = width
        self.height = height                #width and height of the button
        self.colour = colour
        self.back_colour = back_colour
        self.text = text                               #button colours and text inside

    def draw(self, font_type):
        pygame.draw.rect(win, self.back_colour, (self.x - 4, self.y - 4, self.width + 8, self.height + 8))  
        mouse_pos = pygame.mouse.get_pos()
        if mouse_pos[0] >= self.x and mouse_pos[0] <= self.x + self.width and mouse_pos[1] >= self.y and mouse_pos[1] <= self.y + self.height:
            pygame.draw.rect(win, self.back_colour, (self.x, self.y, self.width, self.height))
        else:
            pygame.draw.rect(win, self.colour, (self.x, self.y, self.width, self.height))  
        button_text = font_type.render(self.text, 1, (255, 255, 255))                       #draws  the buttons with the text inside them
        button_text_width = button_text.get_width()
        button_text_height = button_text.get_height()
        win.blit(button_text, (int(self.x + self.width/2 - (button_text_width/2)), int(self.y + self.height/2 - (button_text_height/2))))

    def find_clicked(self):
        if event.type == pygame.MOUSEBUTTONDOWN:
            click = pygame.mouse.get_pos()
            if click[0] >= self.x and click[0] <= self.x + self.width and click[1] >= self.y and click[1] <= self.y + self.height:          #determines if a button has been clicked and returns true if it has
                return True
            else:
                return False


class game(object):

	def __init__(self):
		self.mode = "main menu"

	def main(self):
		if self.mode == "main menu":
			game.menu(self)
		elif self.mode == "one player":
			game.one_player(self)
		elif self.mode == "two player":
			game.two_player(self)

	def menu(self):
		game.draw_menu(self)
		input1 = one_player_button.find_clicked()
		input2 = two_player_button.find_clicked()
		if input1 == True:
			self.mode = "one player"
		elif input2 == True:
			self.mode = "two player"

	def draw_menu(self):
		win.fill((255, 255, 255))
		one_player_button.draw(font2)
		two_player_button.draw(font2)
		menu_text = font1.render("Tic Tac Toe", 1, (12, 14, 12))
		win.blit(menu_text, (400 - int(menu_text.get_width()/2), 50))
		pygame.display.update()

	def one_player(self):
		game.draw_one_player(self)
		input1 = quit_button.find_clicked()
		if input1 == True:
			self.mode = "main menu"

	def draw_one_player(self):
		win.fill((255, 255, 255))
		quit_button.draw(font2)
		pygame.display.update()

	def two_player(self):
		game.draw_two_player(self)
		input1 = quit_button.find_clicked()
		if input1 == True:
			self.mode = "main menu"

	def draw_two_player(self):
		win.fill((255, 255, 255))
		quit_button.draw(font2)
		pygame.display.update()

new_game = game()
one_player_button = button(200, 195, 400, 90, (100, 100, 100), (70, 70, 70), "One Player")
two_player_button = button(200, 330, 400, 90, (100, 100, 100), (70, 70, 70), "Two Player")
quit_button = button(25, 25, 120, 45, (100, 100, 100), (70, 70, 70), "Quit") 

clock = pygame.time.Clock()


while True:
    clock.tick(FPS)
    for event in pygame.event.get():
        if event.type == QUIT:
            pygame.quit()
            sys.exit()
    new_game.main()
    