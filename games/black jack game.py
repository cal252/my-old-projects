import random
import time

def pick_card():
    card = cards[random.randint(0,12)]
    return card
    
def card_graphics(card):
    print(" " + "-"*10)
    print("|", card, " "*(7-len(card)), "|")
    for x in range(5):
        print("|", " "*8, "|")
    print("|", " "*(7-len(card)), card, "|")
    print(" " + "-"*10)

def player():
    total = 0
    count = 0
    stats = []
    carry_on = True
    while carry_on == True:
        answer = input("Would you like to stick (S) or twist (T): ").upper()
        if answer == "T":
            count = count + 1
            time.sleep(1.0)
            new_card = pick_card()
            total = total + new_card[1]
            card_graphics(new_card[2])
            time.sleep(2.0)
            print("You got a", new_card[0], "that has a value of", new_card[1])
            if total > 21:
                print("BUST! You went over 21")
                total = 0
                stats = [count, total]
                return stats
            else:
                print("Your total is", total)
        elif answer == "S":
            print("You are sticking with", total)
            stats = [count, total]
            return stats
        else:
            carry_on = True

def computer():
    c_total = 0
    c_count = 0
    c_stats = []
    keep_going = True
    while keep_going == True:
        if c_total < 12:
            c_count = c_count + 1
            c_new_card = pick_card()
            c_total = c_total + c_new_card[1]
        elif c_total == 12:
            get = chance(50, 1)
            if get == 0:
                c_count = c_count + 1
                c_new_card = pick_card()
                c_total = c_total + c_new_card[1]
                if c_total > 21:
                    c_total = 0
                    c_stats = [c_count, c_total]
                    return c_stats
            else:
                c_stats = [c_count, c_total]
                return c_stats
        elif c_total > 12 and c_total < 16:
            get = chance(9, (21 - c_total))
            if get == 1:
                c_count = c_count + 1
                c_new_card = pick_card()
                c_total = c_total + c_new_card[1]
                if c_total > 21:
                    c_total = 0
                    c_stats = [c_count, c_total]
                    return c_stats
            else:
                c_stats = [c_count, c_total]
                return c_stats
        elif c_total > 15 and c_total < 19:
            get = chance(10, (21 - c_total))
            if get == 1:
                c_count = c_count + 1
                c_new_card = pick_card()
                c_total = c_total + c_new_card[1]
                if c_total > 21:
                    c_total = 0
                    c_stats = [c_count, c_total]
                    return c_stats
            else:
                c_stats = [c_count, c_total]
                return c_stats
        elif c_total > 18 and c_total < 21:
            get = chance(20, (21 - c_total))
            if get == 1:
                c_count = c_count + 1
                c_new_card = pick_card()
                c_total = c_total + c_new_card[1]
                if c_total > 21:
                    c_total = 0
                    c_stats = [c_count, c_total]
                    return c_stats
            else:
                c_stats = [c_count, c_total]
                return c_stats
        else:
            c_stats = [c_count, c_total]
            return c_stats
            
            
def chance(denom, num):
    a = random.randint(1, num)
    b = random.randint(1, denom)
    if a == b:
        return 1
    else:
        return 0

def game():
    player_stats = player()
    player_total = player_stats[1]
    player_count = player_stats[0]
    computer_stats = computer()
    computer_total = computer_stats[1]
    computer_count = computer_stats[0]
    game_stats = [player_total, computer_total, player_count, computer_count]
    return game_stats
        
#2D list, holds data in order - string/character, value(int), character only   
cards = [["Ace", 1, "A"], ["2", 2, "2"], ["3", 3, "3"], ["4", 4, "4"],
         ["5", 5, "5"], ["6", 6, "6"], ["7", 7, "7"], ["8", 8, "8"], ["9", 9, "9"],
         ["10", 10, "10"], ["Jack", 10, "J"], ["Queen", 10, "Q"], ["King", 10, "K"]]

print("Welcome to Black Jack! The aim of the game is to get as close to 21 as possible without going over.")
print("\n\n\n")
player_score = 0
computer_score = 0

playing = True
while playing == True:
    stats = game()
    print("\n")
    time.sleep(2.0)
    if stats[0] > stats[1]:
        player_score = player_score + 1
        print("You won that round, you got", stats[0], "and the computer got", stats[1])
    elif stats[1] > stats[0]:
        computer_score = computer_score + 1
        print("You lost that round, you got", stats[0], "and the computer got", stats[1])
    elif stats[0] == stats[1]:
        if stats[2] > stats[3]:
            player_score = player_score + 1
            print("You won that round, you and the computer got", stats[0], "but you had", stats[2], "cards and the computer had", stats[3], "cards")
        elif stats[3] > stats[2]:
            ccomputer_score = computer_score + 1
            print("You lost that round, you and the computer got", stats[0], "but you had", stats[2], "cards and the computer had", stats[3], "cards")
        else:
            player_score = player_score + 1
            computer_score = computer_score + 1
            print("Draw! You and the computer got", stats[0], "and had", stats[2], "cards")
    play = input("would you like to play another round? (Y/N) ").upper()
    print("\n")
    if play == "Y":
        playing = True
    else:
        playing = False

print("Total scores - ")
print("               Your score:", player_score)
print("               Computer score:", computer_score)
print("\n\n\nThanks for playing!")


        
